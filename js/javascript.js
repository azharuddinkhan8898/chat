$(function() {
    var digits = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
    var randomid = "";
    var connctID;
    var peer;
    localStorage.setItem("connectedwith", "");
    $("#userName, #join input").on("focus", function() {
        $(this).parent().find(".line").css("width", "100%");
    });
    $("#userName, #join input").on("blur", function() {
        $(this).parent().find(".line").css("width", "0%");
    });
    $.urlParam = function(name) {
        var results = new RegExp('[\#&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        } else {
            return results[1] || 0;
        }
    }
    var paramId = $.urlParam('id');
    for (i = 0; i < 10; i++) {
        var rt = digits[Math.floor(Math.random() * 62)];
        randomid += rt;
    }
    if (paramId == null) {
        $(".host").show();
    } else {
        $(".join").show();
        connctID = paramId;
    }

    $("#start").click(function() {
        if ($("#userName").val()) {
            runpeer();
        } else {
            alert("Please enter name");
        }
    });
    $("#join button").click(function() {
        if ($("#join input").val()) {
            runpeer();
        } else {
            alert("Please enter name");
        }
    });

    function runpeer() {
        peer = new Peer(randomid, { host: 'azhar-peerjs.herokuapp.com', port: 80, path: '/' });
        peer.on('open', function(id) {
            console.log('My peer ID is: ' + id);
            $("#userName").val(window.location.href + "#id=" + id).prop('readonly', true);
            $("#userName").click(function() {
                selectText();
            });
            $(".input-form .line").hide();
            $("#start").addClass("btn-success").attr("id", "copy").empty().append("Copy URL");

        });

        connpeer(connctID);
        localStorage.setItem("connectedwith", connctID);

        peer.on('error', function(err) {
            if (err.type == 'unavailable-id') {
                alert("id is taken");
            }
        });
        peer.on('connection', function(conn) {
            console.log(conn.peer + " is connected with you");
            if (localStorage.getItem("connectedwith") != conn.peer) {
                connpeer(conn.peer);
            }

            conn.on('data', function(data) {
                //console.log('Received', data);
                $(".msgs ul").append('<li class="another">'+data+'</li>')
            });
        });
    }

    function connpeer(connthis) {
        conn = peer.connect(connthis);
        conn.on('open', function() {
            console.log("connectd with " + connthis);
            $(".container > div").hide();
            $("#chatbox").show();
        });
    }

    $("#sendbtn").click(function(){
      var getmsg = $("#msg").val();
      conn.send(getmsg);
      $(".msgs ul").append('<li>'+getmsg+'</li>');
      $("#msg").val("");
    });

    function selectText() {
        $("#userName").select();
    }
});